var keystone = require('keystone');

var modelMap = {
  'general': 'GeneralEnquiry',
  'job': 'JobEnquiry',
  'product': 'ProductEnquiry',
  'investment': 'InvestmentEnquiry',
  'agent': 'SalesAgentEnquiry'
};
var messageMap = {
  'GeneralEnquiry': 'Please fill in the following information for us to provide better assistance.',
  'JobEnquiry': 'Please tell us more about you.',
  'ProductEnquiry': 'Great! In order for us to assist you better in your enquiry we require you to fill out the following information. Rest assured that this information will be kept private and confidential.',
  'InvestmentEnquiry': 'Thanks for your interest. Please help us know you better and we\'ll get back to you real soon.',
  'SalesAgentEnquiry': 'Please tell us more about you.'
};

exports = module.exports = function(req, res) {
  
  var view = new keystone.View(req, res),
    locals = res.locals;
  
  var purpose = req.param('purpose') || 'general';
  var query = req.param('query');

  var enquiryModel = modelMap[purpose] || 'GeneralEnquiry';
  var enquiryMessage = messageMap[enquiryModel] || messageMap['GeneralEnquiry'];

  var Enquiry = keystone.list(enquiryModel);
  var enquiryFields = Object.keys(Enquiry.fields).join(',');

  // Set locals
  locals.section = 'contact';
  locals.formData = {};
  locals.validationErrors = {};
  locals.messages = {
    info: [enquiryMessage]
  };

  if (purpose === 'job') {
    if (!query) {
      locals.messages.error = ['Job ID is not specified'];
    } else {
      view.query('job', keystone.list('Job').model.findOne({_id: query}));
    }
  } else if (purpose === 'product') {
    if (!query) {
      locals.messages.error = ['Product ID is not specified'];
    } else {
      view.query('product', keystone.list('Product').model.findOne({_id: query}));
      // for selections
      locals.contactMethods = Enquiry.fields.contactMethod.ops;
    }
  }
  
  // On POST requests, add the Enquiry item to the database
  view.on('post', { action: 'contact' }, function(next) {
    
    var newEnquiry = new Enquiry.model(),
      updater = newEnquiry.getUpdateHandler(req);
    
    updater.process(req.body, {
      flashErrors: true,
      fields: enquiryFields,
      errorMessage: 'There was a problem submitting your enquiry:'
    }, function(err) {
      if (err) {
        locals.validationErrors = err.errors;
        locals.messages.error = ['Opps.. Something went wrong.'];
      } else {
        locals.messages.success = ['Thanks for getting in touch.'];
      }

      // clear the info message
      locals.messages.info = [];

      next();
    });
    
  });
  
  view.render('enquiries/' + enquiryModel);
  
};
