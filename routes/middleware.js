/**
 * This file contains the common middleware used by your routes.
 * 
 * Extend or replace these functions as your application requires.
 * 
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */

var _ = require('underscore'),
  querystring = require('querystring'),
  keystone = require('keystone'),
  jade = require('jade');


/**
  Initialises the standard view locals
  
  The included layout depends on the navLinks array to generate
  the navigation in the header, you may wish to change this array
  or replace it with your own templates / logic.
*/

exports.initLocals = function(req, res, next) {
  
  var locals = res.locals;
  
  locals.navLinks = [
    {label: 'Products', id: 'products', href: '/products'},
    {label: 'About Us', id: 'about', href: '/about'},
    {label: 'Support', id: 'support', href: '/support'},
    {label: 'Join Us', id: 'join', href: '/join'},
    {label: 'Contact Us', id: 'contact', href: '/contact'}
  ];
  
  locals.user = req.user;

  // helpers
  locals.renderTemplate = jade.renderFile;
  
  next();
  
};


/**
  Fetches and clears the flashMessages before a view is rendered
*/

exports.flashMessages = function(req, res, next) {
  
  var flashMessages = {
    info: req.flash('info'),
    success: req.flash('success'),
    warning: req.flash('warning'),
    error: req.flash('error')
  };

  res.locals.messages = res.locals.messages || {};

  Object.keys(flashMessages).forEach(function(key) {
    var messages = flashMessages[key];
    
    // this is to ensure that all will at least have an empty array
    res.locals.messages[key] = (res.locals.messages[key] || []).concat(messages || []);
  });
  
  next();
  
};


/**
  Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
  
  if (!req.user) {
    req.flash('error', 'Please sign in to access this page.');
    res.redirect('/keystone/signin');
  } else {
    next();
  }
  
};


/**
  For simple routes' use
*/

exports.getSimpleRoute = function(opts) {
  
  return function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
    
    // locals.section is used to set the currently selected
    // item in the header navigation.
    locals.section = opts.section || opts.view;

    if (opts.queries) {
      // e.g. {products: []}
      // or {products: function(req) {return keystone.list('Product').model.find()}}
      Object.keys(opts.queries).forEach(function(key) {
        var query = opts.queries[key];

        if (typeof query == 'function') {
          view.query(key, query(req, res));

        } else if (typeof query.fn == 'function') {
          var promise = view.query(key, query.fn(req, res));

          if (query.mapKey) {
            // this is an undocumented feature, visit their source code for more info
            promise.then(function(error, results, next) {
              if (error) {
                return next();
              }

              var mapKey = query.mapKey;

              res.locals[key + "_map"] = results.reduce(function(map, result) {
                map[result[mapKey]] = result;
                return map;
              }, {});

              next();
            });
          }

        } else {
          view.query(key, query);
        }

      });
    }
    
    // Render the view
    view.render(opts.view);
  };
  
};
