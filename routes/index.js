/**
 * This file is where you define your application routes and controllers.
 * 
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 * 
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 * 
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 * 
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 * 
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var _ = require('underscore'),
	keystone = require('keystone'),
	middleware = require('./middleware'),
	importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// NOTE: only define routes with complex operations in individual file

	app.get('/', middleware.getSimpleRoute({
		view: 'index',
		queries: {
			webContent: {
				fn: function(req, res) {
					return keystone.list('WebContent').model.find();
				},
				mapKey: 'name'
			},
			products: function(req) {
				return keystone.list('Product').model.find();
			}
		}
	}));

	app.get('/about', middleware.getSimpleRoute({
		view: 'about',
		queries: {
			webContent: {
				fn: function(req, res) {
					return keystone.list('WebContent').model.find();
				},
				mapKey: 'name'
			}
		}
	}));

	app.get('/support', middleware.getSimpleRoute({
		view: 'support',
		queries: {
			faqs: function(req) {
				return keystone.list('FAQ').model.find();
			},
			guides: function(req) {
				return keystone.list('Guide').model.find();
			}
		}
	}));

	app.get('/join', middleware.getSimpleRoute({
		view: 'join',
		queries: {
			jobs: function(req) {
				return keystone.list('Job').model.find();
			}
		}
	}));

	app.get('/products', middleware.getSimpleRoute({
		view: 'products',
		queries: {
			products: function(req) {
				return keystone.list('Product').model.find();
			}
		}
	}));

	app.get('/product/:id', middleware.getSimpleRoute({
		view: 'product',
		section: 'products',
		queries: {
			product: function(req) {
				return keystone.list('Product').model.findOne({_id: req.param('id')});
			},
			products: function(req) {
				return keystone.list('Product').model.find();
			}
		}
	}));

	app.all('/contact', routes.views.contact);
	
	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
};
