## Interested in investing?

We believe that we have a 1-2 year lead in modern end to end cloud enterprise management systems, judging by new releases from all the top 10 global vendors.

Our current business environment has no parallel due to the Singapore government’s productivity drive.

We require investors with extensive experience & networks that are able to help us develop our young management team. We need people that can teach us how to grow as a team and build a world class organization.

We want to be the #1 global player in the enterprise SaaS industry as we believe that there exists a lot of untapped network efforts that we have only just begun to unlock. Enterprise SaaS will become like a utility, a power plant that supplies software instead of electricity… and everybody will need to have it in order to even compete.

This is however a “winner takes all” market, you can never rest on your laurels until you have truly won. We need to be #1.

As such, we are betting fully on maximising our structural cost advantages and unique positioning in the market.

We are seeing over 500% year on year growth since starting sales in June 2013 and have employee onboarding processes stabilized as well as a large commission based sales agent network. Right now, we want to step on the gas, hard.

Our time to recover customer acquisition costs (CAC) is under 5 months, vs 15-24 months for Netsuite and Salesforce