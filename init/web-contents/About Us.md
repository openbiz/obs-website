## So what is the motivation behind doing what we do?

Most companies in singapore rarely have staff who are constantly on the look out for new technologies that could help improve their business operations.

The problem in Singapore arises because all the local ERP software vendors with credibility are resellers of either Microsoft Dynamics or SAP and they depend on being able to charge you extra for customisation work, which can easily double the quoted price.
Negotiations generally commence after the vendor has implemented your accounts.

So the root of the problem is that the yearly license fees goes directly to the foreign manufacturers and the local vendors usually only manage to charge 2 to 3 years of maintenance fees before the client stops requiring on-demand support.
The business model of traditional ERP systems is to make you pay for more customisations, or pay for a software upgrade a few years down the line.

By leveraging off modern cloud infrastructure and open source technologies, our ActionNest ERP platform reduces our cost of customisation and deployment significantly.
This allows us to offer free customizations as well as fixed price projects.

We want to make it as painless as possible for businesses to improve their productivity.

### Who are the people behind this?

_Coming soon!_