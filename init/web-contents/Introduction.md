### Easy enterprise management solutions for Singaporean companies.

Our client relationships are defined by three core values:

1. ALWAYS PROVIDING THE BEST

    - We only offer the best products on the market, packaged with first-rate deployment and customer support.
    - Software upgrades are free.

1. MAKING IT EASY

    - We care about making your job easier.
    - Customisations are free.

1. MANAGING YOUR RISK

    - We take on the majority of the project risk to give you peace of mind.
    - 90 day refund policy.
