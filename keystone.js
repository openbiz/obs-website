// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');
  

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.


keystone.init({

  'name': 'Open Business Solutions',
  'brand': 'Open Business Solutions',

  'less': 'public',
  'static': 'public',
  'favicon': 'public/favicon.ico',
  'views': 'templates/views',
  'view engine': 'jade',
  
  'auto update': true,
  'session': true,
  'auth': true,
  'user model': 'User',
  'cookie secret': '?d:K@^P/&A0yKy4#q@m2i:zNluNP`P!&L:>pOZVaI$iq>#~y<fT.3e@{rS{m[:e.',

  // http://keystonejs.com/docs/configuration/#options-database
  'mongo': process.env.MONGO_URI || 'mongodb://localhost/obs-website',

  'port': process.env.PORT || 3000,

  'gmail user': process.env.GMAIL_USER,
  'gmail pass': process.env.GMAIL_PASS

});

// ensure that certain configs are set
['gmail user', 'gmail pass'].forEach(function(config) {
  if (!keystone.get(config)) {
    throw 'A required configuration is not being set in the .env: ' + config;
  }
});

// Load your project's Models

keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
  _: require('underscore'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable
});

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
  'users': 'users',
  'contents': ['faqs', 'guides', 'web-contents'],
  'products': 'products',
  'enquiries': ['general-enquiries', 'product-enquiries', 'investment-enquiries', 'job-enquiries', 'sales-agent-enquiries'],
  'jobs': 'jobs'
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();
