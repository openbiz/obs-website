## Deployment (on fresh instance)

1. Install MongoDB **as a service**
1. Clone this project into a dir
1. Open a terminal
1. CD into the dir
1. Execute `npm install`
    1. If your're not logged in as the root, execute `sudo npm install --unsafe-perm`
1. Create a new file **.env** with the content
``` properties
  MONGO_URI=mongodb://user:pass@127.0.0.1/admin
  PORT=80
  GMAIL_USER=mail
  GMAIL_PASS=password
```
1. Execute `forever start keystone.js`

#### Update a deployment

1. Shut down the server
    1. Execute `forever list` and you should see the process' ID
    1. Execute `forever stop <pid>`
1. Update the code base
    1. Execute `git pull`
    1. Execute `npm install` (in case of dependency updates)
1. Restart the server

## Development

1. Install MongoDB
    - If not installed as a service, remember to execute `mongod` everytime
1. Clone this project into a dir
1. Open a terminal
1. CD into the dir
1. Execute `npm install`
    - It should does a `bower install` too
    - If not, you will need to do that manually
1. Create a new file **.env** with the content
``` properties
  GMAIL_USER=mail
  GMAIL_PASS=password
```
1. Execute `npm start` or `node keystone`
1. Browse to http://localhost:3000


blahblah test