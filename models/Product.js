var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Product Model
 * ==========
 */

var Product = new keystone.List('Product', {
  autokey: {path: 'slug', from: 'name', unique: true}
});

Product.add({
  name: { type: Types.Text, required: true, index: true },
  caption: { type: Types.Markdown, default: 'No caption available' },
  thumbnail: {type: Types.LocalFile, dest: "public/images/products"},
  // shouldn't be editable in the admin ui for now
  detailsTemplateName: {type: Types.Text, noedit: true}
});


/**
 * Registration
 */

Product.defaultColumns = 'name, label';
Product.register();
