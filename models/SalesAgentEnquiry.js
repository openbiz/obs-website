var keystone = require('keystone');
var Types = keystone.Field.Types;
var factory = require('./EnquiryModelFactory');

/**
 * Sales Agent Enquiry Model
 * =============
 */

factory.create({
  name: 'SalesAgentEnquiry',
  emailSubject: 'New sales agent enquiry'
});