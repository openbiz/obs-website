var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * WebContent Model
 * ==========
 * This is where we store generic web content
 */

var WebContent = new keystone.List('WebContent', {
  autokey: {path: 'slug', from: 'name', unique: true}
});

WebContent.add({
  name: { type: Types.Text },
  content: { type: Types.Markdown },
  createdAt: { type: Date, default: Date.now }
});


/**
 * Registration
 */

WebContent.defaultSort = 'name';
WebContent.defaultColumns = 'name, createdAt';
WebContent.register();
