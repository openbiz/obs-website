var keystone = require('keystone'),
  Types = keystone.Field.Types;

var path = require('path');
var nodemailer = require('nodemailer');
var jade = require('jade');
var moment = require('moment');
var _ = require('underscore');

var gmailUser = keystone.get('gmail user');

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: gmailUser,
    pass: keystone.get('gmail pass')
  }
});

/**
 * Generates Enquiry Model
 * =============
 */

module.exports = {

  create: function(opts) {

    var Enquiry = new keystone.List(opts.name, opts.listOpts || {
      nocreate: true,
      noedit: true
    });

    // default
    Enquiry.add({
      name: { type: Types.Text, required: true },
      email: { type: Types.Email, required: true },
      phone: { type: Types.Text },
      website: { type: Types.Url },
      message: { type: Types.Markdown, required: true },
      createdAt: { type: Date, default: Date.now }
    });

    // custom
    if (opts.fields) {
      Enquiry.add(opts.fields);
    }

    Enquiry.schema.pre('save', function(next) {
      this.wasNew = this.isNew;
      next();
    })

    Enquiry.schema.post('save', function() {
      if (this.wasNew) {
        this.sendNotificationEmail();
      }
    });

    var sendMail = function sendMail(emails, locals, callback) {
      // load template
      var template = path.join(__dirname, '..', 'templates', 'emails', opts.name + '.jade');
      var html = jade.renderFile(template, locals);

      var mailOpts = {
        from: keystone.get('name') + ' <' + gmailUser + '>',
        to: emails,
        subject: opts.emailSubject || 'New Enquiry',
        html: html
      };

      transporter.sendMail(mailOpts, callback);
    };

    Enquiry.schema.methods.sendNotificationEmail = function(callback) {
      
      var enquiry = this;
      
      keystone.list('User').model.find().where('isAdmin', true).exec(function(err, admins) {
        
        if (err) return callback(err);

        var locals = {
          enquiry: enquiry,
          _: _,
          moment: moment
        };

        var emails = admins.map(function(admin) {
          return admin.email;
        });

        if (typeof opts.populateEmailLocals == 'function') {
          // this gives an interception point to different type of enquiries
          opts.populateEmailLocals(locals, function() {
            sendMail(emails, locals, callback);
          });
        } else {
          sendMail(emails, locals, callback);
        }
        
      });
      
    };

    Enquiry.defaultSort = opts.defaultSort || '-createdAt';
    Enquiry.defaultColumns = 'name, email, createdAt' + (opts.defaultColumns ? ', ' + opts.defaultColumns : '');
    Enquiry.register();

    return Enquiry;

  }

};
