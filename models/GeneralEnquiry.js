var factory = require('./EnquiryModelFactory');

/**
 * (General) Enquiry Model
 * =============
 */

factory.create({
	name: 'GeneralEnquiry'
});