var keystone = require('keystone');
var Types = keystone.Field.Types;
var factory = require('./EnquiryModelFactory');

/**
 * Job Enquiry Model
 * =============
 */

factory.create({
  name: 'JobEnquiry',
  fields: {
    job: { type: Types.Relationship, ref: 'Job', required: true }
    // TODO: resume attachment
  },
  defaultColumns: 'job',
  emailSubject: 'New job enquiry',
  populateEmailLocals: function populateEmailLocals(locals, next) {
    var enquiry = locals.enquiry;
    var jobId = enquiry.job;

    if (!jobId) {
      return next(locals);
    }

    keystone.list('Job').model.findOne({_id: jobId}, function(error, job) {
      if (!error) {
        locals.job = job;
      }

      next(locals);
    });
  }
});