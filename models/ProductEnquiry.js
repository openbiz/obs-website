var keystone = require('keystone');
var Types = keystone.Field.Types;
var factory = require('./EnquiryModelFactory');

/**
 * Product Enquiry Model
 * =============
 */

factory.create({
  name: 'ProductEnquiry',
  fields: {
    company: { type: Types.Text },
    businessNature: { type: Types.Text },
    staffCount: { type: Types.Text },
    currentSystem: { type: Types.Text },
    currentSystemChallanges: { type: Types.Textarea },
    contactMethod: { type: Types.Select, options: [{value: 'email', label: 'Via email'}, {value: 'phone', label: 'Via phone'}] },
    product: { type: Types.Relationship, ref: 'Product', required: true }
  },
  defaultColumns: 'product',
  emailSubject: 'New product enquiry',
  populateEmailLocals: function populateEmailLocals(locals, next) {
    var enquiry = locals.enquiry;
    var productId = enquiry.product;

    if (!productId) {
      return next(locals);
    }

    keystone.list('Product').model.findOne({_id: productId}, function(error, product) {
      if (!error) {
        locals.product = product;
      }

      next(locals);
    });
  }
});