var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * FAQ Model
 * ==========
 */

var FAQ = new keystone.List('FAQ', {
  autokey: {path: 'slug', from: 'question', unique: true}
});

FAQ.add({
  question: { type: Types.Text, required: true, initial: true },
  answer: { type: Types.Markdown }
});


/**
 * Registration
 */

FAQ.defaultColumns = 'question';
FAQ.register();
