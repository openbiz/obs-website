var keystone = require('keystone');
var Types = keystone.Field.Types;
var factory = require('./EnquiryModelFactory');

/**
 * Investment Enquiry Model
 * =============
 */

factory.create({
  name: 'InvestmentEnquiry',
  emailSubject: 'New investment enquiry'
});