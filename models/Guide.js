var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Guide Model
 * ==========
 */

var Guide = new keystone.List('Guide', {
  autokey: {path: 'slug', from: 'name', unique: true}
});

Guide.add({
  name: { type: Types.Text, required: true, initial: true },
  link: { type: Types.Url },
  content: { type: Types.Markdown }
});


/**
 * Registration
 */

Guide.defaultColumns = 'name';
Guide.register();
