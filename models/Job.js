var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Job Model
 * ==========
 */

var Job = new keystone.List('Job', {
  autokey: {path: 'slug', from: 'title', unique: true}
});

Job.add({
  title: { type: Types.Text, required: true, index: true, initial: true },
  location: { type: Types.Textarea },
  responsibilities: { type: Types.Markdown },
  requirements: { type: Types.Markdown },
  createdAt: { type: Date, default: Date.now }
});


/**
 * Registration
 */

Job.defaultColumns = 'title';
Job.register();
