var path = require('path');
var fs = require('fs');

var imgPath = path.join('public', 'images', 'products');

// note: caption needs to be in object literal because we're creating the products manually
var list = [
  {
    name: 'Goods trading, distribution & logistics', 
    caption: {md: 'Your workflows revolve around creating Quotations, Sales Orders, Procurement Orders, and Delivery Orders.\n\nYou need to accurately manage inventory, automate your document workflow and improve your profit margins.'},
    thumbnail: {
      'filename': 'goods.png',
      'path': imgPath,
      'size': (fs.statSync(path.join(imgPath, 'goods.png'))).size,
      'filetype': 'image/png'
    },
    detailsTemplateName: 'goods-trading-distribution-logistics'
  },
  {
    name: 'Professional Services',
    caption: {md: 'Your business revolves around selling fixed price, cost plus or complex contracts that are run as projects.\n\nYou need to manage your project costs with precision.'},
    thumbnail: {
      'filename': 'services.png',
      'path': imgPath,
      'size': (fs.statSync(path.join(imgPath, 'services.png'))).size,
      'filetype': 'image/png'
    },
    detailsTemplateName: 'professional-services'
  },
  {
    name: 'Food & Beverage',
    caption: {md: 'You run a restaurant, bar, club or any F&B related outlet.\n\nYou want daily reports on sales by menu item and raw materials utilised in order to calculate your cost of goods sold.'},
    thumbnail: {
      'filename': 'fnb.png',
      'path': imgPath,
      'size': (fs.statSync(path.join(imgPath, 'fnb.png'))).size,
      'filetype': 'image/png'
    },
    detailsTemplateName: 'food-beverage'
  }
];

var keystone = require('keystone');
var Product = keystone.list('Product');
var async = require('async');

// TODO: fix the `updateItem` function in keystone's localfile.js so that it can create localfile instances from json
// instead of having to do it manually in an update script
module.exports = function(done) {
  async.forEach(list, function(data, next) {
    var product = new Product.model(data);

    product.save(function(error) {
      if (error) {
        console.error("Failed to create product: " + error);
      }

      next(error);
    });
  }, done);
};