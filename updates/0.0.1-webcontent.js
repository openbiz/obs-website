var fs = require('fs');
var path = require('path');

var webContents = [];

var webContentFolder = path.join(__dirname, '..', 'init', 'web-contents');
var files = fs.readdirSync(webContentFolder);

files.forEach(function(file) {
  var name = path.basename(file, path.extname(file));
  var content = fs.readFileSync(path.join(webContentFolder, file), {encoding: 'utf8'});

  webContents.push({
    name: name,
    content: content
  });
});

exports.create = {
  WebContent: webContents
};