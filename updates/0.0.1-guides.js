exports.create = {
  Guide: [
    {
      name: 'Sales Agents Program',
      link: 'https://drive.google.com/open?id=0B9nIItZIj1yQWXlNSmRhQnJuWUU&authuser=0'
    },
    {
      name: 'ERPNext Manual - Setup',
      link: 'https://drive.google.com/open?id=0B9nIItZIj1yQYUZIenlkVDZ1YTg&authuser=0'
    },
    {
      name: 'ERPNext Manual - Buying Module',
      link: 'https://drive.google.com/open?id=0B9nIItZIj1yQcHdfTEtzSzMtbkk&authuser=0'
    },
    {
      name: 'ERPNext Manual - Selling Module',
      link: 'https://drive.google.com/open?id=0B9nIItZIj1yQSFc0ZUFqenhJR28&authuser=0'
    },
    {
      name: 'ERPNext Manual - Human Resource Module',
      link: 'https://drive.google.com/open?id=0B9nIItZIj1yQUktpREM4ZXBVYUE&authuser=0'
    },
    {
      name: 'CorePOS Technical Guide',
      link: 'https://drive.google.com/open?id=0B9nIItZIj1yQMnc5YlNzM3EtdG8&authuser=0'
    }
  ]
};