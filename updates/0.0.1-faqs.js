exports.create = {
  FAQ: [
    {
      question: 'What is an ERP system?',
      answer: 'An ERP (Enterprise Resource Planning) system is a business process management software which consists of integrated modules related to human resources, accounting management, warehousing and other services.' + 
              '\n\nThe ERP system integrates these business modules into one complete system to help streamline business processes and information across the entire organization, thus increasing the efficiency and competence of the company.'
    },
    {
      question: 'What is ERPNext?',
      answer: 'ERPNext is an online cloud-based ERP application that aims to help small to medium enterprises manage and oversee their organizations efficiently. It is a web-based application which is user friendly and easy to use, with simple looking module icons to navigate around.'
    },
    {
      question: 'What are the main features of ERPNext?',
      answer: 'It includes base modules such as financial accounting, human resource management, manufacturing, inventory management, support management and much more.'
    },
    {
      question: 'What is Cloud-based? Why is it recommended?',
      answer: '“Cloud-based” means that your application is hosted online such that any authorized user may access it via an Internet connection. There are advanced options of Public Cloud vs Private Cloud - that is primarily for enhanced security reasons.'
    },
    {
      question: 'Does ERPNext allow import/export of data?',
      answer: 'Yes, it allows import and export of data on .csv format. Other formats such as Excel may be configured and supported as well.'
    },
    {
      question: 'Can I customize a form or field in my ERPNext?',
      answer: 'Yes, we provide customization for each individual company and their business process flow. We understand that different businesses have different information structure and therefore, by customizing, it will be more convenient and efficient for you to integrate your business processes in your ERPNext system.'
    },
    {
      question: 'How do I add users into my ERPNext system?',
      answer: 'You can add users by simply entering their name and a valid email. And there you go!'
    },
    {
      question: 'Who can access my ERPNext system?',
      answer: 'When adding users, you can assign different roles and set permissions in your system. You can easily select different types of permission for your users in the system. By setting permissions, you can limit access for your users to specific documents or forms.' + 
              '\n\nWe understand that some of your company’s credential or information is fully confidential. Therefore, our system allows you to control the amount of information each user can view and/or edit. For instance, an Accounts Manager should view the accounts receivable and payables whereas an Account User should only view the accounts payable.'
    },
    {
      question: 'Will my company be eligible to apply for the Productivity and Innovation Credit (PIC) scheme?',
      answer: 'Will my company be eligible to apply for the Productivity and Innovation Credit (PIC) scheme?' + 
              '\n\nFor more information on PIC, please visit [this link](http://www.iras.gov.sg/irasHome/page04.aspx?id=10572#Applying_for_Cash_Payout)'
    }
  ]
};